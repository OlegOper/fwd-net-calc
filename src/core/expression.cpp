#include "expression.h"

namespace calc {

double ExpressionBase::applyOperation(double left, Lexer::Token operation, double right) {
    if (Lexer::Plus == operation) {
        return left + right;
    } else if (Lexer::Minus == operation) {
        return left - right;
    } else if (Lexer::Multiply == operation) {
        return left * right;
    } else if (Lexer::Divide == operation) {
        if (right == 0) {
            throw BadOperation("Zero division");
        }
        return left / right;
    }
    throw BadOperation(std::string("Unexpected operation \"") + Lexer::tokenToChar(operation) + "\"");
}

void StreamExpression::addToken(Lexer::Token token) {
    if (isFinished()) {
        invalidToken(token, "Expression is finished");
    }
    if (isSubexprStart(token)) {
        expressions_.push_back(StreamBasicExpression());
        isInitialized_ = false;
        lastNumber_ = false;
        return;
    }

    if (!lastNumber_ && isInitialized_) {
        invalidToken(token, "Two consequent operations in expression");
    }

    if (isSubexprEnd(token)) {
        if (!lastNumber_) {
            invalidToken(token, "Closing not started subexpression");
        }
        double res = expressions_.back().getResult();
        expressions_.pop_back();
        if (expressions_.empty()) {
            invalidToken(token, "Closing not started subexpression");
        }
        isInitialized_ = true;
        lastNumber_ = false;
        addNumber(res);
        return;
    } else if (isEnd(token)) {
        if (!lastNumber_ || expressions_.size() > 1) {
            invalidToken(token, "Unexpected end of expression");
        }
        isFinished_ = true;
    }

    if (!isInitialized_ && !isPlusOrMinus(token)) {
        invalidToken(token, "First token can't be operation or end of expression");
    }
    expressions_.back().addToken(token);

    lastNumber_ = false;
    isInitialized_ = true;

}

void StreamExpression::addNumber(double value) {
    if (isFinished()) {
        invalidNumber(value, "Expression is finished");
    }
    if (lastNumber_ && isInitialized_) {
        invalidNumber(value, "Two consequent numbers in expression");
    }
    expressions_.back().addNumber(value);
    lastNumber_ = true;
    isInitialized_ = true;
}

double StreamExpression::getResult() {
    if (!isFinished()) {
        throw BadOperation("Expression is completed");
    }
    return expressions_.back().getResult();
}

} // namespace calc

