#pragma once

#include "symbols_stream.h"

#include <cstdint>
#include <stdexcept>
#include <string>
#include <unordered_map>

namespace calc {

class BadToken: public std::runtime_error {
    using runtime_error::runtime_error;
};

class Lexer {
public:
    enum Token {
        Number = 0,
        Plus,
        Minus,
        Multiply,
        Divide,
        LeftBracket,
        RightBracket,
        NewLine,
        Whitespace,
        EndOfStream
    };

    Lexer(SymbolsStream& stream): stream_(stream)
    {
    }

    static char tokenToChar(Token token);

    bool hasNext() {
        return streaming_ || stream_.hasNext();
    }

    Token next();

    int32_t getLastNumber() {
        return lastNumber_;
    }

private:
    Token nextRaw();

    Token parseSymbol(char symbol);

    int32_t symbolToDigit(char symbol) {
        return symbol - '0';
    }

    int32_t lastNumber_ = 0;
    bool streaming_ = false;
    char lastChar_;
    SymbolsStream& stream_;
};

} // namespace calc

