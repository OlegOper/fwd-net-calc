#pragma once

#include "lexer.h"

#include <cstdint>
#include <memory>
#include <stdexcept>
#include <vector>

namespace calc {

class BadOperation: public std::runtime_error {
    using runtime_error::runtime_error;
};

class ExpressionBase {
protected:
    bool isEnd(Lexer::Token token) {
        return Lexer::NewLine == token || Lexer::EndOfStream == token;
    }

    bool isSubexprStart(Lexer::Token token) {
        return Lexer::LeftBracket == token;
    }

    bool isSubexprEnd(Lexer::Token token) {
        return Lexer::RightBracket == token;
    }

    bool isOperation(Lexer::Token token) {
        return isMultOrDiv(token) || isPlusOrMinus(token);
    }

    bool isMultOrDiv(Lexer::Token token) {
        return Lexer::Multiply == token || Lexer::Divide == token;
    }

    bool isPlusOrMinus(Lexer::Token token) {
        return Lexer::Plus == token || Lexer::Minus == token;
    }

    void invalidToken(Lexer::Token token, const std::string& why = "") {
        throw BadToken("Invalid token " + std::to_string(token) + " " + why);
    }

    void invalidNumber(double number, const std::string& why = "") {
        throw BadToken("Invalid token " + std::to_string(number) + " " + why);
    }

    double applyOperation(double left, Lexer::Token operation, double right);
};

class StreamBasicExpression: public ExpressionBase {
public:
    void addToken(Lexer::Token token) {
        operation_ = token;
    }

    void addNumber(double value) {
        number_ = applyOperation(number_, operation_, value);
    }

    double getResult() {
        return number_;
    }

private:
    double number_ = 0;
    Lexer::Token operation_ = Lexer::Plus;
};

class StreamExpression: public ExpressionBase {
public:
    StreamExpression() {
        expressions_.push_back(StreamBasicExpression());
    }

    void addToken(Lexer::Token token);

    void addNumber(double value);

    double getResult();

    bool isFinished() {
        return isFinished_;
    }

private:
    void checkIsNotFinished(Lexer::Token token) {
        if (isFinished()) {
            invalidToken(token, "Expression is finished");
        }
    }
    std::vector<StreamBasicExpression> expressions_;
    bool lastNumber_ = false;
    bool isFinished_ = false;
    bool isInitialized_ = false;
};

} // namespace calc

