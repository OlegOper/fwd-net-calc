#include "symbols_stream.h"

namespace calc {

char InputSymbolsStream::next() {
    char nextChar;
    data_ >> std::noskipws >> nextChar;
    if (!data_) {
        return '\0';
    }
    return nextChar;
}

} // namespace calc

