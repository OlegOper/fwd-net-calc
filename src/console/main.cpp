#include <iostream>

#include <core/symbols_stream.h>
#include <core/lexer.h>
#include <core/parser.h>

int main() {
    calc::InputSymbolsStream sss(std::cin);

    calc::Lexer lexer(sss);

    calc::StreamParser parser;
    auto res = parser.calculate(lexer);

    for (auto it : res) {
        std::cout << "Res = " << it << std::endl;
    }
}

