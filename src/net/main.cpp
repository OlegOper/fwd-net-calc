#include "server.h"

#include <cstdlib>
#include <algorithm>
#include <cctype>
#include <functional>
#include <iostream>
#include <stdexcept>
#include <thread>
#include <utility>
#include <vector>

#include <boost/asio.hpp>
#include <boost/lexical_cast.hpp>

int parsePort(const char* port) {
    try {
        int res = boost::lexical_cast<unsigned int>(port);
        if (res < 1 || res > 65535) {
            throw std::runtime_error("Invalid port");
        }
        return res;
    } catch (const boost::bad_lexical_cast&) {
        throw std::runtime_error("Invalid port");
    }
}

int main(int argc, char* argv[]) {
    try {
        if (argc != 2) {
            std::cerr << "Usage: server <port>\n";
            return 1;
        }

        boost::asio::io_service io_service;
        calc::Server server(io_service, parsePort(argv[1]));

        const unsigned maxThreads = std::max(std::thread::hardware_concurrency(), 1u);
        std::vector<std::thread> threads;
        for (unsigned i = 0; i < maxThreads; ++i) {
            const auto func = static_cast<size_t (boost::asio::io_service::*)()>(&boost::asio::io_service::run);
            threads.push_back(std::thread(std::bind(func, &io_service)));
        }

        for (auto& thread : threads) {
            thread.join();
        }
    }
    catch (std::exception& e) {
        std::cerr << "Exception: " << e.what() << "\n";
    }

    return 0;
}
