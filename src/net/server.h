#pragma once

#include <sstream>
#include <string>
#include <boost/asio.hpp>

#include <core/symbols_stream.h>
#include <core/lexer.h>
#include <core/parser.h>


namespace calc {

using boost::asio::ip::tcp;

class SessionBase {
public:
    SessionBase(std::size_t bufferSize): buffer_(bufferSize, '\0') {
    }

protected:
    std::size_t readToLastNumber(std::size_t length);

    void readToEnd(std::size_t readEnd, std::size_t length);

    void processBuffer(std::size_t length, std::string& res);

    std::string makeResponse(std::istream& input);

    boost::asio::streambuf streambuf_;
    std::string buffer_;
    calc::StreamParser parser_;
};

class Session: public SessionBase, public std::enable_shared_from_this<Session> {
public:
    static constexpr std::size_t kBufferSize = 1024;
    Session(tcp::socket socket): SessionBase(kBufferSize), socket_(std::move(socket)) {
    }

    void start() {
        read();
    }

private:
    void read();

    void write(std::size_t length);

    tcp::socket socket_;
    std::string response_;
};

class Server {
public:
    Server(boost::asio::io_service& io_service, int port):
        acceptor_(io_service, tcp::endpoint(tcp::v4(), port)),
        socket_(io_service)
    {
        accept();
    }

private:
    void accept() {
        acceptor_.async_accept(socket_,
            [this](boost::system::error_code ec) {
                if (!ec) {
                    std::make_shared<Session>(std::move(socket_))->start();
                }
                accept();
            });
    }

    tcp::acceptor acceptor_;
    tcp::socket socket_;
};

} // namespace calc

