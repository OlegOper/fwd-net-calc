#include "server.h"

#include <iostream>

namespace calc {

std::size_t SessionBase::readToLastNumber(std::size_t length) {
    while (length > 0 && std::isdigit(buffer_[length - 1])) {
        --length;
    }
    if (!length) {
        return 0;
    }

    std::ostream os(&streambuf_);
    os << buffer_.substr(0, length);
    return length;
}

void SessionBase::readToEnd(std::size_t readEnd, std::size_t length) {
    if (readEnd == length) {
        return;
    }
    std::ostream os(&streambuf_);
    os << buffer_.substr(readEnd, length - readEnd);
}

void SessionBase::processBuffer(std::size_t length, std::string& res) {
    auto readEnd = readToLastNumber(length);

    if (readEnd > 0) {
        std::istream input(&streambuf_);
        res = makeResponse(input);
    } else {
        res = {};
    }
    readToEnd(readEnd, length);
}

std::string SessionBase::makeResponse(std::istream& input) {
    try {
        std::string s(std::istreambuf_iterator<char>(input), {});
        StringSymbolsStream sss(s);

        Lexer lexer(sss);

        auto res = parser_.calculate(lexer);

        std::stringstream stringRes;
        for (auto it : res) {
            stringRes << it << '\n';
        }
        return stringRes.str();
    } catch (std::runtime_error& e) {
        return e.what();
    }
}

void Session::read() {
    auto self(shared_from_this());

    socket_.async_read_some(boost::asio::buffer(&buffer_[0], buffer_.size()),
        [this, self](boost::system::error_code ec, std::size_t length) {
            if (!ec) {
                write(length);
            }
        });
}

void Session::write(std::size_t length) {
    auto self(shared_from_this());

    processBuffer(length, response_);

    boost::asio::async_write(socket_, boost::asio::buffer(response_),
        [this, self](boost::system::error_code ec, std::size_t /*length*/) {
            if (!ec) {
                read();
            }
        });
}

} // namespace calc
