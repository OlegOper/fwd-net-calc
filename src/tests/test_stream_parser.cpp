#include <boost/test/unit_test.hpp>

#include <core/parser.h>
#include <core/symbols_stream.h>

#include <string>
#include <tuple>
#include <vector>

using Token = calc::Lexer::Token;

BOOST_AUTO_TEST_CASE(StreamParserCanCalculateOperations) {
    std::vector<std::tuple<std::string, double>> testSet = {
        {"7 + 3", 10},
        {"7 - 3", 4},
        {"7 * 3", 21},
        {"5 / 2", 2.5},
    };

    for (auto testItem : testSet) {
        double res = std::get<1>(testItem);
        calc::StringSymbolsStream sss(std::get<0>(testItem));
        calc::Lexer lexer(sss);
        calc::StreamParser parser;

        BOOST_CHECK(parser.calculate(lexer).empty());
        BOOST_CHECK_EQUAL(parser.endOfStream(), res);
    }
}

BOOST_AUTO_TEST_CASE(StreamParserCanProcessMultipleExpressions) {
    std::string expression = "1 + 2\n5 * 6";
    const std::vector<double> expected = {3, 30};

    calc::StringSymbolsStream sss(expression);
    calc::Lexer lexer(sss);
    calc::StreamParser parser;

    auto res = parser.calculate(lexer);
    res.push_back(parser.endOfStream());
    BOOST_CHECK_EQUAL(res.size(), 2);
    BOOST_CHECK_EQUAL(res[0], expected[0]);
    BOOST_CHECK_EQUAL(res[1], expected[1]);
}

BOOST_AUTO_TEST_CASE(StreamParserCanProcessMultipleMessages) {
    std::string expression1 = "1 + ";
    std::string expression2 = "2 + 3";

    calc::StringSymbolsStream sss1(expression1);
    calc::Lexer lexer1(sss1);
    calc::StringSymbolsStream sss2(expression2);
    calc::Lexer lexer2(sss2);
    calc::StreamParser parser;

    BOOST_CHECK(parser.calculate(lexer1).empty());
    BOOST_CHECK(parser.calculate(lexer2).empty());
    BOOST_CHECK_EQUAL(parser.endOfStream(), 6);
}

BOOST_AUTO_TEST_CASE(StreamParserThrowsOnBadExpression) {
    std::string expression = "1 + abc";

    calc::StringSymbolsStream sss(expression);
    calc::Lexer lexer(sss);
    calc::StreamParser parser;

    BOOST_CHECK_THROW(parser.calculate(lexer), calc::BadToken);
}
