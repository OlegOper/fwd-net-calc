#include <boost/test/unit_test.hpp>

#include <net/server.h>

#include <algorithm>
#include <sstream>
#include <string>

class TestSession: public calc::SessionBase {
public:
    static constexpr std::size_t kBufferSize = 10;
    TestSession(): SessionBase(kBufferSize) {
    }

    std::string processInput(const std::string& buffer) {
        if (buffer.size() > kBufferSize) {
            throw std::runtime_error("Bad size");
        }
        std::copy(buffer.begin(), buffer.end(), buffer_.begin());
        std::string res;
        processBuffer(buffer.size(), res);
        return res;
    }
};

BOOST_AUTO_TEST_CASE(TestSessionHandleSingleExpression) {
    TestSession session;
    BOOST_CHECK_EQUAL(session.processInput("1 + 2\n"), "3\n");
    BOOST_CHECK_EQUAL(session.processInput("(1+2)*3\n"), "9\n");
}

BOOST_AUTO_TEST_CASE(TestSessionHandleLongExpressions) {
    TestSession session;
    BOOST_CHECK_EQUAL(session.processInput("1 + 2"), "");
    BOOST_CHECK_EQUAL(session.processInput(" * 3\n"), "9\n");
}

BOOST_AUTO_TEST_CASE(TestSessionHandleContinuousNumbers) {
    TestSession session;
    BOOST_CHECK_EQUAL(session.processInput("1 + 234"), "");
    BOOST_CHECK_EQUAL(session.processInput("56 - 3\n"), "23454\n");
}
