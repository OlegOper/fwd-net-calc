#include <boost/test/unit_test.hpp>

#include <core/symbols_stream.h>

#include <sstream>
#include <string>


BOOST_AUTO_TEST_CASE(StringSymbolsStreamReturnsAllData) {
    const std::string data = "String symbols stream data";
    calc::StringSymbolsStream sss(data);

    for (char c : data) {
        BOOST_TEST(sss.hasNext());
        BOOST_CHECK_EQUAL(sss.next(), c);
    }

    BOOST_TEST(!sss.hasNext());
}

BOOST_AUTO_TEST_CASE(InputSymbolsStreamReturnsAllData) {
    const std::string data = "String symbols stream data";
    std::stringstream ss(data);
    calc::InputSymbolsStream sss(ss);

    for (char c : data) {
        BOOST_TEST(sss.hasNext());
        BOOST_CHECK_EQUAL(sss.next(), c);
    }

    BOOST_TEST(sss.hasNext());
    BOOST_CHECK_EQUAL(sss.next(), '\0');
    BOOST_TEST(!sss.hasNext());
}
