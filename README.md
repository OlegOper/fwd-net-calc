# HOWTO:

## Requirements

* C++14 compliant compiler
* Boost 1.61+
* CMake 3.1.0+

## Build boost:
b2 --with-system --with-thread --with-date_time --with-regex --with-serialization --with-test stage

## Bulid:

```
mkdir -p Release
cd Release
cmake -DCMAKE_BUILD_TYPE=Release .. && make
```

## Run:

```
./Release/netcalc <port number>
```

## Generate expressions

```
cd generator
python .
```
